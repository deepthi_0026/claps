/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { DashboardWrapper } from "./styled";

const Dashboard = () => {
  return (
    <DashboardWrapper>
      <header className="aui-main-header aui-pri-header">
        <a href="www.heart.org" className="aui-skip-content">
          Skip to main content
        </a>
        <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto">
          <a href="/" className="claps-logo" aria-label="Claps Logo" />
          <button
            className="navbar-toggler ml-2 px-0"
            type="button"
            data-toggle="collapse"
            data-target="#toggleNav"
            aria-controls="toggleNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="aha-icon-hamburger" />
          </button>
          <div
            className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
            id="toggleNav"
          >
            <ul className="navbar-nav mx-lg-3 flex-lg-row flex-column">
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Home</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Upcoming Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/rewards">Rewards</a>
                </button>
              </li>
              <button
                className="navbar-toggler ml-2 px-0"
                type="button"
                data-toggle="collapse"
                data-target="#toggleNav1"
                aria-controls="toggleNav1"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="aha-icon-hamburger" />
              </button>
              <div
                className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
                id="toggleNav1"
              />
            </ul>
          </div>
        </nav>
      </header>
      <div className="d-flex flex-column flex-grow-1">
        <div className="bgcolor d-flex justify-content-between">
          <h1>Employee Dashboard</h1>
          <div className="createbutton">
            <Button variant="contained" color="default">
              <Link to="/createreward">Create Rewards</Link>
            </Button>
          </div>
        </div>
        <div className="d-lg-flex container-fluid">
          <div className="rewards-section ml-4 flex-grow-1">
            <div className="row row-cols-1 row-cols-md-3 pt-5">
              <div className="col mb-4">
                <div className="card p-3">
                  <div className="row">
                    <div className="text-center col-lg-6 col-md-12 p-1">
                      <img
                        src="../images/emp2.png"
                        className="card-img-top"
                        alt="Employee"
                      />
                    </div>
                    <div className="empcontent col-lg-6 col-md-12 p-1">
                      <p className="heading mb-0">Tom Vans</p>
                      <p className="designation mb-0">Software Engineer</p>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <h5 className="card-title">
                      <img
                        src="../images/award.svg"
                        alt="Award"
                        className="px-2"
                      />
                      Best Performer of the Year
                    </h5>
                    <p className="card-text">
                      This is the award for the best performer who is XYZ for
                      his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card p-3">
                  <div className="row">
                    <div className="text-center col-lg-6 col-md-12 p-1">
                      <img
                        src="../images/emp2.png"
                        className="card-img-top"
                        alt="Employee"
                      />
                    </div>
                    <div className="empcontent col-lg-6 col-md-12 p-1 ">
                      <p className="heading mb-0">Mark Thomas</p>
                      <p className="designation mb-0">UI Designer</p>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <h5 className="card-title">
                      <img
                        src="../images/award.svg"
                        alt="Award"
                        className="px-2"
                      />
                      Best Team
                    </h5>
                    <p className="card-text">
                      This is the award for the best team who is XYZ for his
                      exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card p-3">
                  <div className="row">
                    <div className="text-center col-lg-6 col-md-12 p-1">
                      <img
                        src="../images/emp2.png"
                        className="card-img-top"
                        alt="Employee"
                      />
                    </div>
                    <div className="empcontent col-lg-6 col-md-12 p-1 ">
                      <p className="heading mb-0">Evan Jefferson</p>
                      <p className="designation mb-0">Product Manger</p>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <h5 className="card-title">
                      <img
                        src="../images/award.svg"
                        alt="Award"
                        className="px-2"
                      />
                      Best Performer of the Year
                    </h5>
                    <p className="card-text">
                      This is the award for the best produt Manager who is XYZ
                      for his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
              <div className="col mb-4">
                <div className="card p-3">
                  <div className="row">
                    <div className="text-center col-lg-6 col-md-12 p-1">
                      <img
                        src="../images/emp2.png"
                        className="card-img-top"
                        alt="Employee"
                      />
                    </div>
                    <div className="empcontent col-lg-6 col-md-12  p-1">
                      <p className="heading mb-0">Harry Styles</p>
                      <p className="designation mb-0">Graphic Designer</p>
                    </div>
                  </div>
                  <div className="card-body pt-0">
                    <h5 className="card-title">
                      <img
                        src="../images/award.svg"
                        alt="Award"
                        className="px-2"
                      />
                      Best Performer of the Year
                    </h5>
                    <p className="card-text">
                      This is the award for the best graphic designer who is XYZ
                      for his exemplary performance in the Project Invoice
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div id="fixedbutton" className="show">
          <img
            src="../images/fixedbutton.svg"
            className="fixed-menu-btn"
            alt="openbtn"
          />
        </div> */}
      </div>
    </DashboardWrapper>
  );
};

export default Dashboard;
