import React from "react";
import AdminFooterWrapper from "./styled";

export const AdminFooter = () => {
  return (
    <AdminFooterWrapper className="text-right">
      <div>
        &copy;2020 American Heart Association, Inc. All rights reserved.
      </div>
      <div>
        Unauthorized use prohibited. The American Heart Association is a
        qualified 501(c)(3) tax-exempt organization
      </div>
    </AdminFooterWrapper>
  );
};
export default AdminFooter;
