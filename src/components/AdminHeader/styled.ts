import styled from "styled-components";

const AdminHeaderWrapper = styled.header`
  .aui-skip-content {
    padding: 16px;
    color: #c10e21;
    position: absolute;
    left: -9999px;
    width: 100%;
    top: 0;
    text-align: center;
    background-color: #fff;
    &:focus {
      left: 0;
      z-index: 500;
    }
  }
  &.aui-pri-header {
    .aui-header-content {
      width: 100%;
    }
    .aui-pri-header-t {
      padding-left: 15px;
      padding-right: 15px;
      @media (min-width: 576px) {
        padding-left: 20px;
        padding-right: 20px;
      }
      @media (min-width: 768px) {
        padding-left: 30px;
        padding-right: 30px;
      }
      @media (min-width: 992px) {
        padding-left: 40px;
        padding-right: 40px;
      }
    }
  }
`;
export default AdminHeaderWrapper;
