import React from "react";
import AdminHeaderWrapper from "./styled";

const Global: any = global;
export const AdminHeader = () => {
  const signout = () => {
    Global.userManager.signoutRedirect();
  };

  return (
    <AdminHeaderWrapper className="aui-main-header aui-pri-header">
      <a href="#maincontent" className="aui-skip-content">
        Skip to main content
      </a>
      <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto aui-pri-header-t">
        <a
          href="/"
          className="aha-certification-logo"
          aria-label="Quality and Certification tool logo"
        >
          Quality and Certification tool logo
        </a>
        <button
          className="navbar-toggler ml-2"
          type="button"
          data-toggle="collapse"
          data-target="#adminHeaderCollapse"
          aria-controls="adminHeaderCollapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="aha-icon-hamburger" />
        </button>
        <div
          className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
          id="adminHeaderCollapse"
        >
          <ul className="navbar-nav ml-lg-3 flex-lg-row flex-column">
            <li className="d-flex nav-item dropdown pl-lg-3 flex-column">
              <button
                type="button"
                className="btn btn-text dropdown-toggle flex-grow-1 text-left nav-link"
                id="accountdropdown"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                My Account
              </button>
              <div
                className="dropdown-menu p-lg-0 aui-header-dropdown"
                aria-labelledby="accountdropdown"
              >
                <div
                  onClick={signout}
                  className="dropdown-item py-2"
                  role="button"
                  onKeyUp={signout}
                  tabIndex={0}
                >
                  Sign Out
                </div>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </AdminHeaderWrapper>
  );
};
export default AdminHeader;
