exports.sendErrorResponse = (result, res) => {
  res.api.success = false;
  res.api.error.code = result.code;
  res.api.error.message = result.message;
  res.api.error.details = result.details;
  res.api.statusCode = result.statusCode;
  res.status(res.api.statusCode);
  res.json(res.api);
};
