const jwt = require("jsonwebtoken");

function generateAccessToken(user) {
  return jwt.sign(user, "JWT_SECRET", { expiresIn: "30m" });
}

module.exports = generateAccessToken;
