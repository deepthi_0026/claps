const RewardNominator = require("../models").reward_nominators;

exports.postRewardNominator = async (rewardData) => {
    const result = await RewardNominator.create(rewardData);
    return result
}