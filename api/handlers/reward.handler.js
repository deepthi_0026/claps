const express = require("express");

const router = express.Router();

const rewardController = require("../controllers/index").reward;

router.use("/rewards", rewardController);

module.exports = router;
