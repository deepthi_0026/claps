const express = require("express");

const router = express.Router();

const rewardNominatorController = require("../controllers/index").rewardNominator;

router.use("/nominators", rewardNominatorController);

module.exports = router;
