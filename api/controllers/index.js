const user = require("./user.controller");
const reward = require("./reward.controller");
const rewardNominator = require("./rewardNominator.controller")
const rewardComment = require("./rewardComment.controller")

module.exports = {
  user,
  reward,
  rewardNominator,
  rewardComment
};
